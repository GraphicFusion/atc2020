(function() {
    tinymce.PluginManager.add('custom_mce_button', function(editor, url) {
        editor.addButton('custom_mce_button', {
            text: 'Add Button',
            icon: 'custom-mce-icon',
onclick: function() {
                editor.windowManager.open({
                    title: 'Insert Button',
                    body: [{
                        type: 'textbox',
                        name: 'buttontext',
                        label: 'Button Text',
                        value: ''
                    },
                    {
                        type: 'textbox',
                        name: 'textboxhref',
                        label: 'Button href',
                        value: ''
                    },
                    {
                        type: 'listbox',
                        name: 'className',
                        label: 'BackgroundType',
                        values: [{
                            text: 'Light',
                            value: 'light-bg'
                        }, {
                            text: 'Dark',
                            value: 'dark-bg'                            
                        }]
                    }],
                    onsubmit: function(e) {
                        editor.insertContent(
                            '<a href="' + e.data.textboxhref + '" class="atc-button ' + e.data.className + '">' + e.data.buttontext + '</a>'
                        );
                    }
                });
            }        });
    });
})();