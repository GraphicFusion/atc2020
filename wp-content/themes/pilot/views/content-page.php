<?php global $pilot; ?>
<?php echo do_shortcode('[mason_build_blocks container=altnav]');?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if( $pilot->use_default_page_titles && !get_field('hide_title') ) : ?>
		<header class="entry-header">
			<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
		</header>
	<?php endif; ?>
	<div class="entry-content">
		<?php the_content(); ?>
		<?php echo do_shortcode('[mason_build_blocks container=content]');?>
	</div><!-- .entry-content -->
	<div style="clear:both;"></div>
	<footer class="entry-footer">
		<div style="clear:both;"></div>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'pilot' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer>
</article>