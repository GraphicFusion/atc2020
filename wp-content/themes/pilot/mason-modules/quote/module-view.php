<?php 
	global $args; 
	$style = get_field('styling_options', 'option');		
	if('default' != $style && $style){
		$style = "";
//		$style = "-".$style;
	}
	else{
		$style = "";
	}
?>
	<div class=" container container-lg container-md container-sm ">
		<div class="row">
			<div class="col-lg-12 quote-container" >
				<div class="nav-wrapper-wrapper">
					<div class="nav-wrapper">
						<img src='<?php bloginfo('template_directory'); ?>/mason-modules/quote/img/Icon-Quote<?php echo $style; ?>.svg'>	
					</div>
				</div>
				<div class="shadow-quote">
				<?php if(count($args['quotes']) > 0 ) : $i = 0; ?>
					<?php foreach($args['quotes'] as $quote):  ?>
					<div class="quote-foot-content">
						<div class="title-wrap">
							<?php if( $quote['title']  ) : ?>
								<div class="content"><?php echo $quote['title']; ?></div>
							<?php endif; ?>
							<?php if(  $quote['caption']  ) : ?>
								<div class="citation"><?php echo $quote['caption']; ?></div>
							<?php endif; ?>
						</div><!--/title-wrap-->
					</div><!--/quote-foot-content-->
					<?php endforeach; ?>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<script>
	$( document ).ready(function() {
		$('#<?php echo $args['id']; ?> .shadow-quote').slick({
			slidesToShow:1,
			dots:false,
			arrows:true,
			appendArrows: $('#<?php echo $args['id']; ?> .nav-wrapper'),
		});


	});
</script>