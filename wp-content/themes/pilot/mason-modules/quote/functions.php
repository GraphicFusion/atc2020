<?php
	function build_quote_layout(){
		global $i;
		$args = [];
		$quotes = [];
		$rows_arr = mason_get_sub_field('quote_block_rows');
		if( is_array($rows_arr) ){
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$quote_args = array(
					'title' => $row_arr['quote_block_quote'],
					'id' => 'quote_block'.'_'.$i,
					'caption' => $row_arr['quote_block_content'],
				);
				$quotes[] = $quote_args;
			}
		}
		$args['quotes'] = $quotes;
		$name = "quote";
		if(get_sub_field($name.'_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field($name.'_block_margin-top');
		}
		if(get_sub_field($name.'_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field($name.'_block_margin-bottom');
		}
		
		if( is_array( $args ) ){
			return $args;
		}
	}
	function quote_admin_enqueue($hook) {
		wp_register_style( 'quote_wp_admin_css', get_template_directory_uri() . '/mason-modules/quote/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'quote_wp_admin_css' );
    }
	add_action( 'admin_enqueue_scripts', 'quote_admin_enqueue' );
?>