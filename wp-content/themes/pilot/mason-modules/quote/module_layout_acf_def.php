<?php
global $pilot;
// add module layout to flexible content


    // add module layout to flexible content 
    $name = "quote";
    $module_layout = array (
        'key' => create_key($name, 'block'),
        'name' => 'quote_block',
        'label' => 'Quote',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => create_key($name,'rows'),
                'label' => 'Rows',
                'name' => 'quote_block_rows',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => '',
                'max' => '',
                'layout' => 'block',
                'button_label' => 'Add Quote Slide',
                'sub_fields' => array (

                    array (
                        'key' => create_key($name,'quote'),
                        'label' => 'Quote',
                        'name' => 'quote_block_quote',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => "",
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'quote_upload' => 1,
                    ),
                    array (
                        'key' => create_key('quote','content'),
                        'label' => 'Citation' ,
                        'name' => 'quote_block_content',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => "",
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'quote_upload' => 1,
                    ),
                ),

            ),
        ),);

?>