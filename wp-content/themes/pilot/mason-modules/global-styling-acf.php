<?php
	function get_styling_fields( $module ){
		$included_modules = [
			'media',
			'slideshow'
		];
		$choices = [
			'0' => '0',
			'30px' => '30px',
			'60px' => '60px',
			'100px' => '100px'
		];
		if(1 == 1){
			$module = $module . "_block_";
			$fields =  array(
				/*
				array(
					'key' => create_key($module,'module_styling'),
					'label' => 'Styling Parameters',
					'name' => $module.'_module_styling',
					'type' => 'true_false',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => 'Hide Styles',
					'ui_off_text' => 'Show Styles',
				),
				*/
				array(
					'key' => create_key($module,'top_margin'),
					'label' => 'Top Margin',
					'name' => $module.'margin-top',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						/*
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
						*/
					),
					'wrapper' => array(
						'width' => '30%',
						'class' => '',
						'id' => '',
					),
	                'choices' => $choices,
	                'default_value' => array(
	                ),
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => create_key($module,'bottom_margin'),
					'label' => 'Bottom Margin',
					'name' => $module.'margin-bottom',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						/*
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
						*/
					),
					'wrapper' => array(
						'width' => '30%',
						'class' => '',
						'id' => '',
					),
	                'choices' => $choices,
	                'default_value' => '100px',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			);
			return $fields;
		}
	}
?>