<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
	$style = get_field('styling_options', 'option');

?>
<?php if($args['alert_active']) : ?>
	<div class="alert-wrapper" style="width:100%;">
		<div class="container ">
			<div class="row">
				<div class='col-lg-12'>
					<div class="alert-text"><?php echo $args['alert_text']; ?></div>
					<?php if( is_array($args['alert_link'])) : ?>
						<div class="alert-link"><a href="<?php echo $args['alert_link']['url']; ?>">
				<img class="menu-open menu-blue" src="<?php echo get_template_directory_uri(); ?>/mason-modules/navigation/images/Icon-Arrow-Right.svg">							
						</a></div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="responsive-navigation ">
	<div class="container-fluid">
		<div class="row">
			<div class="phone">
				<a href="tel:1-<?php echo $args['phone']; ?>"><?php echo $args['phoneDisplay']; ?></a>
			</div>
			<div class="mobile-button-wrapper">
				MENU
				<img class="menu-open menu-blue" src="<?php echo get_template_directory_uri(); ?>/mason-modules/navigation/images/Icon-Menu.svg">
			</div>
		</div>
	</div>
</div>
<div class="navigation" id="navigation_id">
	<div class="container-fluid container container-lg">
		<div class="row">
				<?php if($args['location']) : ?>
					<div class="col-lg-3">
				<?php else : ?>
					<div class="col-lg-2">
				<?php endif; ?>
				<div class="logo-wrapper">			
					<a href="/">
						<img class="logo" src="<?php echo $args['logo']['url']; ?>">
					</a>
					<?php if($args['location']) : ?>
						<div class="logo-location">
							<a href="/">
								<span><?php echo $args['location']; ?></span>
							</a>
						</div>
					<?php endif; ?>
				</div>
			</div>
				<?php if($args['location']) : ?>
					<div class="col-lg-9">
				<?php else : ?>
					<div class="col-lg-10">
				<?php endif; ?>
				<div class="main-nav">
					<?php $active_link_subs = ""; if( count($args['main_links'])>0): ?>
						<ul class="primary-nav">
							<?php foreach($args['main_links'] as $main): 
								$main_href = $main['navigation_block_main_link']['url'];
								$active_class = "";
								$active_link = 0;
								if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
									$active_class = " active";
									$active_link = 1;
								}
								$is_parent_class ="";
							?>
								<?php 
									$subs = "";
									if( is_array($main['navigation_block_sublinks']) && count($main['navigation_block_sublinks'])>0): 
										$is_parent_class = "menu-item-has-children";
								?>
									<?php foreach($main['navigation_block_sublinks'] as $sub ) : 
										if( isset($sub['navigation_block_sub_link']['url'])){
											$sub_href = $sub['navigation_block_sub_link']['url'];
											if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
												$active_class = " active";
												$active_link = 1;
											}
											$subs .= '<li><a href="'.$sub_href.'">'.$sub['navigation_block_sub_link']['title'].'</a></li>';
										}
									endforeach; ?>
								<?php 
									if( $active_link && $subs){
										$active_link_subs = $subs;
									}
									endif; 
								?>
								<li class="<?php echo $active_class. " ". $is_parent_class." ".$style; ?>"><a href="<?php echo $main_href; ?>">
										<?php echo $main['navigation_block_main_link']['title']; ?>
									</a>
									<?php if($subs) : ?>
										<ul class="sub-nav">
											<li class="main-link">
												<a href="<?php echo $main_href; ?>">
													<?php echo $main['navigation_block_main_link']['title']; ?>
												</a>
											</li>
											<?php echo $subs; ?>
										</ul>
									<?php endif; ?>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="mobile-navigation">
	<div class="mobile-menu">
		<div class="">	
			<?php if( count($args['main_links'])>0): ?>
				<div class="">
					<?php foreach($args['main_links'] as $main): 
						$main_href = $main['navigation_block_main_link']['url'];
						$active_class = "";
						if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
							$active_class = " active";
						}
					?>
						<?php $subs = ""; if( is_array($main['navigation_block_sublinks']) && count($main['navigation_block_sublinks'])>0): ?>
							<?php foreach($main['navigation_block_sublinks'] as $sub ) : 
								if( isset($sub['navigation_block_sub_link']['url'])){
									$sub_href = $sub['navigation_block_sub_link']['url'];
									if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
										$active_class = " active";
									}
									$subs .= '<li><a href="'.$sub_href.'">'.$sub['navigation_block_sub_link']['title'].'</a></li>';
								}
							endforeach; ?>
						<?php endif; ?>
						<div class="menu-item-wrapper <?php echo $active_class; ?>">
							<a href="<?php echo $main_href; ?>">
								<?php echo $main['navigation_block_main_link']['title']; ?>
							</a>
							<?php if($subs) : ?>
								<ul class="sub-nav">
									<?php echo $subs; ?>
								</ul>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div><!--/what-->
