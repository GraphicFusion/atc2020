<?php
	function build_navigation_layout(){
		$args = array(
			'alert_text' => mason_get_sub_field('navigation_block_alert_text'),
			'alert_link' => mason_get_sub_field('navigation_block_alert_link'),
			'alert_active' => mason_get_sub_field('navigation_block_alert_visibility'),
			'phoneDisplay' => mason_get_sub_field('navigation_block_phoneDisplay'),
			'phone' => mason_get_sub_field('navigation_block_phone'),
			'logo' => mason_get_sub_field('navigation_block_logo'),
			'location' => mason_get_sub_field('navigation_block_location'),
			'main_links' => mason_get_sub_field('navigation_block_main_nav')
		);

		return $args;
	}
?>