<?php 
	global $args; 
	$style = get_field('styling_options', 'option');

?>
	<div class=" container container-lg container-md container-sm ">
		<div class="row">
			<div class="col-lg-12">
				<div class="form-container <?php echo $style; ?>" >
					<?php echo do_shortcode("[gravityform id='" . $args['form'] . "' description='false']"); ?>
				</div>
			</div>
		</div>
	</div>
