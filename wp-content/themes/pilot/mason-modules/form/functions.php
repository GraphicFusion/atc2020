<?php
	function build_form_layout(){
		global $i;
		$args = [];
		$forms = [];
		$args['form'] = mason_get_sub_field('form_block_form');
		$args['module_styles'] = [];
		if(get_sub_field('form_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('form_block_margin-top');
		}
		if(get_sub_field('form_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('form_block_margin-bottom');
		}
		
		if( is_array( $args ) ){
			return $args;
		}
	}
?>