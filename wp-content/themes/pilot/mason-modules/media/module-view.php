<?php
    global $args;
    $block_name = 'media_block_';   
    $block_id = $args['acf_incr'];
    unset($args['acf_incr']);
?>
<div id="slick-slider-<?php echo $block_id; ?>" style="">
    <?php foreach ( $args['slides'] as $arg ) : $slide_id = $arg[$block_name . "slide_id"]; 
            ?>
        <?php if( isset( $arg[$block_name . 'bg_video_file_mp4'] ) || isset( $arg[$block_name . 'image']  ) ) : ?>
            <div class="img-block-wrap video" id="<?php echo $slide_id; ?>" 
                <?php if($arg[$block_name . 'image']) : ?>
                        style="background-image: url(<?php echo $arg[$block_name . 'image']['url']; ?>);"
                <?php endif; ?>
            >
                    <div class="img-overlay"> 
                        <div class="filter" 
                        <?php if( $arg[$block_name . 'overlay_color'] ) :
                            list($r, $g, $b) = sscanf($arg[$block_name . 'overlay_color'], "#%02x%02x%02x"); 
                            $outColor = $r . ", " . $g . "," . $b .",";
                        ?>
                        style="background-image:linear-gradient(to right, rgba(<?php echo $outColor; ?> <?php echo $arg[$block_name . 'start_opacity']; ?>) 0%, rgba(<?php echo $outColor; ?> 0) 100% );"
                        <?php endif; ?>
                        >
                        </div>

                        <div class="media-container" >
                            <div class="slide-content">
                                <div class="lower-content">
                                    <div class="container-fluid container-card container-lg">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <?php if( isset( $arg[$block_name . 'title'] ) && $arg[$block_name . 'title'] ) : ?>                            
                                                    <h1><?php echo $arg[$block_name . 'title']; ?></h1>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-xs-12 col-md-6"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6 subtitle">
                                                <?php if( isset( $arg[$block_name . 'subtitle'] ) && $arg[$block_name . 'subtitle'] ) : ?>                            
                                                    <p><?php echo $arg[$block_name . 'subtitle']; ?></p>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-xs-6"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div><!--/media-container-->
                    </div><!--/img-overlay-->
                </div><!--/img-block-wrap-->
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div><!--/slick-slider-->

