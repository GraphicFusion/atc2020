<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	$one = $args['link_one'];
	$two = $args['link_two'];
	$three = $args['link_three'];
	$style = get_field('styling_options', 'option');		
	if('default' != $style && $style){
		$arrow = "Icon-Arrow-Right.svg";
		//$arrow = "Icon-Arrow-Right-".$style.".svg";
	}
	else{
		$arrow = "Icon-Arrow-Right.svg";
	}

?>
<div class="container">
	<div class="row <?php echo $args['num']; ?>">
		<div class="col-lg-12">
			<div class="drop-shadow">
				<?php if('one' == $args['num'] ) : ?>
					<div class="single">
						<a href='<?php echo $one['url']; ?>' target='<?php echo $one['target']; ?>'>
							<span><?php echo $one['title']; ?></span>
						<img src='<?php bloginfo('template_directory'); ?>/dest/images/<?php echo $arrow; ?>'>
						</a>
					</div>
				<?php elseif( 'two' == $args['num'] ) : ?>
					<div class="double border first">
						<a href='<?php echo $one['url']; ?>' target='<?php echo $one['target']; ?>'>
							<span><?php echo $one['title']; ?></span>
							<img src='<?php bloginfo('template_directory'); ?>/dest/images/<?php echo $arrow; ?>'>
						</a>
					</div>
					<div class="double">
						<a href='<?php echo $two['url']; ?>' target='<?php echo $two['target']; ?>'>
							<span><?php echo $two['title']; ?></span>
							<img src='<?php bloginfo('template_directory'); ?>/dest/images/<?php echo $arrow; ?>'>						
						</a>
					</div>
				<?php elseif('three' == $args['num']) : ?>
					<div class="triple border first">
						<a href='<?php echo $one['url']; ?>' target='<?php echo $one['target']; ?>'>
							<span><?php echo $one['title']; ?></span>
							<img src='<?php bloginfo('template_directory'); ?>/dest/images/<?php echo $arrow; ?>'>
						</a>
					</div>
					<div class="triple border">
						<a href='<?php echo $two['url']; ?>' target='<?php echo $two['target']; ?>'>
							<span><?php echo $two['title']; ?></span>
							<img src='<?php bloginfo('template_directory'); ?>/dest/images/<?php echo $arrow; ?>'>
						</a>
					</div>
					<div class="triple">
						<a href='<?php echo $three['url']; ?>' target='<?php echo $three['target']; ?>'>
							<span><?php echo $three['title']; ?></span>
							<img src='<?php bloginfo('template_directory'); ?>/dest/images/<?php echo $arrow; ?>'>							
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>