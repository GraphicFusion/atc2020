<?php
	function build_callout_layout(){
		$args = array(
			'title' => mason_get_sub_field('callout_block_title'),
			'num' => mason_get_sub_field('callout_block_num'),
			'link_one' => mason_get_sub_field('callout_block_link_one'),
			'link_two' => mason_get_sub_field('callout_block_link_two'),
			'link_three' => mason_get_sub_field('callout_block_link_three'),
		);
		$args['module_styles'] = [];
		if(get_sub_field('callout_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('callout_block_margin-top');
		}
		if(get_sub_field('callout_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('callout_block_margin-bottom');
		}

		return $args;
	}
?>