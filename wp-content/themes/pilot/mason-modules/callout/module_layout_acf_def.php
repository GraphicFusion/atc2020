<?php
	global $pilot;
	// add module layout to flexible content 
	$name = "callout";
	$module_layout = array (
		'key' => create_key($name,'block'),
		'name' => $name.'_block',
		'label' => 'Call-Out',
		'display' => 'block',
		'sub_fields' => array (
			array(
		        'key' => create_key($name,'num'),
				'label' => 'Number of Columns',
				'name' => $name . '_block_num',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'one' => 'One',
					'two' => 'Two',
					'three' => 'Three',
				),
				'default_value' => array(
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'return_format' => 'value',
				'ajax' => 0,
				'placeholder' => '',
			),
			array(
		        'key' => create_key($name,'link_one'),
				'label' => 'Column One - Link',
				'name' =>  $name . '_block_link_one',
				'type' => 'link',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '33',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
			),
			array(
		        'key' => create_key($name,'link_two'),
				'label' => 'Column Two - Link',
				'name' =>  $name . '_block_link_two',
				'type' => 'link',
				'instructions' => '',
				'required' => 0,
				'wrapper' => array(
					'width' => '33',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'conditional_logic' => array(
					array(
						array(
							'field' => create_key($name,'num'),
							'operator' => '==',
							'value' => 'two',
						),
					),
					array(
						array(
							'field' => create_key($name,'num'),
							'operator' => '==',
							'value' => 'three',
						),
					),
				),
			),
			array(
		        'key' => create_key($name,'link_three'),
				'label' => 'Column Three - Link',
				'name' =>  $name . '_block_link_three',
				'type' => 'link',
				'instructions' => '',
				'required' => 0,
				'wrapper' => array(
					'width' => '33',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'conditional_logic' => array(
					array(
						array(
							'field' => create_key($name,'num'),
							'operator' => '==',
							'value' => 'three',
						),
					),
				),
			),			
		),
		'min' => '',
		'max' => '',
	);
?>