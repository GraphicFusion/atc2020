<?php global $args;  ?>
<div class="container container-lg container-md container-sm">
	<div class="row blog-container">
		<?php if( !array_key_exists('blogs' , $args )) : $post = $args['blog']; ?>		
			<div class="col-lg-12">
				<div class="img-block-wrap">
					<img class="blog-image" src=" <?php echo $post->url; ?>">
				</div><!--img-block-wrap-->
				<div class="title-wrap">
					<div class="caption"><?php echo $args['caption']; ?></div>
					<h3 class="blog-title"><?php echo $post->post_title; ?></h3>
					<a class="atc-button" href="<?php echo $post->permalink; ?>">Read</a>
				</div><!--/title-wrap-->
			</div>
		<?php endif; ?>
	</div>
	<?php if( array_key_exists('blogs' , $args )) : ?>		
		<div class="row load-container">
			<div class="col-lg-12">
				<a class='atc-button' id='load_more' href='' data-id="<?php echo $args['id']; ?>">Load More</a>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php if( array_key_exists('blogs' , $args )) : ?>		

	<script>
	jQuery(document).ready(function($) {
			if( !window.ajaxPage ){
				window.ajaxPage = 1;
			}
			if( !window.blogModules ){
				window.blogModules = [];
			}
			window.blogModules.push('<?php echo $args['id']; ?>' );
		})
	</script>
<?php endif; ?>