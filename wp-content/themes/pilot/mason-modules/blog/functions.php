<?php
	function build_blog_layout(){
		global $i;
		$args = [];
		if( 'list' == mason_get_sub_field('blog_block_list') ){
			$blogs = [];
			$postArgs = array(
			  'numberposts' => 10
			);
			 
			$latest_posts = get_posts( $postArgs );
			$args['blogs'] = $latest_posts;
		}
		else{
			$args['caption'] = mason_get_sub_field('blog_block_caption');
			$post_id = mason_get_sub_field('blog_block_post');
			$blog = get_post($post_id);
			$blog->permalink = get_permalink($blog->ID);
			if( $nail = get_post_thumbnail_id($blog->ID) ){
				$thumb_url_array = wp_get_attachment_image_src($nail, 'medium', true);
				$blog->url = $thumb_url_array[0];
			}
			else{
				$blog->url = "";				
			}
			if(!$args['caption']){
				$date = strtotime( $blog->post_date );
				$args['caption'] = date( 'F j, Y', $date );
			}
			$args['blog'] = $blog;
		}
		$args['module_styles'] = [];
//		print_r($args);
		if(get_sub_field('blog_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('blog_block_margin-top');
		}
		if(get_sub_field('blog_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('blog_block_margin-bottom');
		}
		
		if( is_array( $args ) ){
			return $args;
		}
	}
	function get_all_news( $args = array() ){
		$news = array();
		$defaults = array(
			'posts_per_page' => 2,
			'paged' => '1',
		);
		$params = array_replace_recursive($defaults, $args);
//print_r($params);
		$args=array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => $params['posts_per_page'],
			'paged' => $params['paged']
		);
		$news_query = new WP_Query($args);
		$news = [];
		if( $news_query->have_posts() ):
			foreach( $news_query->posts as $post ){
				$post->permalink = get_permalink($post->ID);
				if( $nail = get_post_thumbnail_id($post->ID) ){
					$thumb_url_array = wp_get_attachment_image_src($nail, 'medium', true);
					$post->url = $thumb_url_array[0];
				}
				else{
					$post->url = "";				
				}

				$date = strtotime( $post->post_date );
				$post->pretty_date = date( 'F j, Y', $date );

				$news[] = $post;
			}
		endif;
		return $news;
	}
//add_action( 'wp_ajax_nopriv_blog', 'get_ajax_news' );
//add_action( 'wp_ajax_blog', 'get_ajax_news' );
function get_ajax_news(){
}

function blog_enqueue() {
wp_enqueue_script( 'ajax_pagination',  get_stylesheet_directory_uri() . '/dest/js/ajax.js', array( 'jquery' ), '1.0', true );
wp_localize_script( 'ajax_pagination', 'ajax_pagination', array(
	'ajaxurl' => admin_url( 'admin-ajax.php' )
));


//wp_enqueue_script( 'ajax-blog', get_template_directory_uri() . '/dest/js/ajax.js', array('jquery') );
   //   wp_localize_script( 'ajax-blog', 'ajax_blog_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
 }

 add_action( 'wp_enqueue_scripts', 'blog_enqueue' );
 add_action( 'wp_ajax_nopriv_ajax_pagination', 'my_ajax_pagination' );
add_action( 'wp_ajax_ajax_pagination', 'my_ajax_pagination' );

function my_ajax_pagination() {
	$params = $_REQUEST['params'];
	$posts_per_page = 10;
	if( $params['posts_per_page'] ){
		$posts_per_page = $params['posts_per_page'];
	}
	$param = array(
		'post_type' => 'post', 
		'posts_per_page' => $posts_per_page, 
		'paged' => $params['paged'], 
	);
	$news = get_all_news( $param );
	$result = json_decode(json_encode($news), true);
	$response['posts'] = $result;
	echo json_encode( $response );
	exit;
}	
?>