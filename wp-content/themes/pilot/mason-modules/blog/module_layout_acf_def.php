<?php
global $pilot;
// add module layout to flexible content


    // add module layout to flexible content 
    $name = "blog";
    $args = array(
        'post_type' => 'post',
        'post_status' => array('publish'),
        'numberposts' => -1    
    );
    $query = new WP_Query($args);
//    print_r($query);
    $posts = [];
    if($query->posts){
        foreach($query->posts as $post){
            $posts[$post->ID] = $post->post_title;
        }
    }
    $module_layout = array (
        'key' => create_key($name, 'block'),
        'name' => 'blog_block',
        'label' => 'Blog/News',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => create_key($name,'list'),
                'label' => 'Featured or List',
                'name' => 'blog_block_list',
                'type' => 'radio',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'featured' => 'Featured',
                    'list' => 'List',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => '',
                'layout' => 'horizontal',

            ),
            array(
                'key' => create_key($name,'caption'),
                'label' => 'Caption',
                'name' => 'blog_block_caption',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'list'),
                            'operator' => '==',
                            'value' => 'featured',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'taxonomy' => '',
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'object',
                'ui' => 1,
            ),                        
            array(
                'key' => create_key($name,'post'),
                'label' => 'Featured Post',
                'name' => 'blog_block_post',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'list'),
                            'operator' => '==',
                            'value' => 'featured',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => $posts,                
                'taxonomy' => '',
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'object',
                'ui' => 1,
            ),            

        ),
    );
?>