<?php
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	//print_r($args);
?>
<div class="container container-lg container-md container-sm">

	<div class="footer-content" style="width:100%; ">
		<!--<div class="menu-close-wrapper-close">
			<div class="menu-button-close">
				<img class="menu-close menu-blue" src="<?php echo get_template_directory_uri(); ?>/mason-modules/navigation/images/ICON-CLOSE-GRAY.svg">						
			</div>
		</div>-->
		<div class="row">
			<div class="col-lg-12">
				<a href='/'>
					<img class='logo' src='<?php echo $args['logo']; ?>'>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="left_container">
					<div id="main_nav_footer_container"></div>
					<div id="other_links">
						<ul>
						<?php foreach ($args['links'] as $key => $array) : ?>
							<li><a href='<?php echo $array['footer_link']['url']; ?>'><?php echo $array['footer_link']['title']; ?></a></li>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>		
				<div class="right_container">
					<div class="content-container">
						<?php echo $args['content']; ?>
					</div>
					<div class="social-container">
						<?php foreach($args['social'] as $social) : ?>
							<a href='<?php echo $social['footer_icon_link']['url']; ?>'><img src='<?php echo $social['footer_icon_logo']['url']; ?>'></a>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<hr>
			</div>
			<div class="col-lg-12">
				<div class="left_container">
					<div class="content-container">
						<?php echo $args['lower_content']; ?>
					</div>
				</div>		
					<div class="copyright-container-mobile">
						<?php echo $args['info']; ?>
					</div>

				<div class="right_container">
					<div class="badges-container">
						<?php if(is_array($args['badges'])) : ?>
							<?php foreach($args['badges'] as $badge) : ?>
								<a href='<?php echo $badge['footer_badge_link']; ?>'><img src='<?php echo $badge['footer_badge_logo']['url']; ?>'></a>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-lg-12">
				<div class="copyright-container">
					<?php echo $args['info']; ?>
				</div>
			</div>
		</div>
	</div>	
</div>