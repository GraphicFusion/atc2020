<?php
	function build_footer_layout(){
		$logo = "";

		if( $img = mason_get_sub_field('footer_block_logo') ){
			$logo = $img['url'];
		};
		$args = array(
			'logo' => $logo,
			'content' => mason_get_sub_field('footer_block_content'),
			'lower_content' => mason_get_sub_field('footer_block_lower_content'),
			'links' => mason_get_sub_field('footer_links'),
			'social' => mason_get_sub_field('footer_social'),
			'badges' => mason_get_sub_field('footer_badges'),
			'info' => mason_get_sub_field('footer_info')			
		);
		$args['module_styles'] = [];
		$name = "footer";
		if(get_sub_field($name.'_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field($name.'_block_margin-top');
		}
		if(get_sub_field($name.'_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field($name.'_block_margin-bottom');
		}

		return $args;
	}
?>