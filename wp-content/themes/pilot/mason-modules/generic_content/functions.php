<?php
	function build_generic_content_layout(){
		$args = array(
			'isHeader' => mason_get_sub_field('generic_content_block_isHeader'),
			'title' => mason_get_sub_field('generic_content_block_title'),
			'use_card' => mason_get_sub_field('generic_content_block_use_card'),
			'col_num' => mason_get_sub_field('generic_content_block_num'),
			'modify_one' => mason_get_sub_field('generic_content_block_modify_one'),
			'svg_one' => mason_get_sub_field('generic_content_block_svg_one'),
			'image_one' => mason_get_sub_field('generic_content_block_image_one'),
			'content_one' => mason_get_sub_field('generic_content_block_content_one'),
			'button_one' => mason_get_sub_field('generic_content_block_button_one'),
			'modify_two' => mason_get_sub_field('generic_content_block_modify_two'),
			'svg_two' => mason_get_sub_field('generic_content_block_svg_two'),
			'image_two' => mason_get_sub_field('generic_content_block_image_two'),
			'content_two' => mason_get_sub_field('generic_content_block_content_two'),
			'button_two' => mason_get_sub_field('generic_content_block_button_two'),
			'modify_three' => mason_get_sub_field('generic_content_block_modify_three'),
			'svg_three' => mason_get_sub_field('generic_content_block_svg_three'),
			'image_three' => mason_get_sub_field('generic_content_block_image_three'),
			'content_three' => mason_get_sub_field('generic_content_block_content_three'),
			'button_three' => mason_get_sub_field('generic_content_block_button_three'),
		);
		$args['module_styles'] = [];
		if(get_sub_field('generic_content_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('generic_content_block_margin-top');
		}
		if(get_sub_field('generic_content_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('generic_content_block_margin-bottom');
		}

		return $args;
	}
?>