<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	//print_r($args);
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
		<?php if('one' == $args['col_num']) : ?>
				<div class="gc-wrap one">
					<div class="gc-content generic">
						<?php if($args['modify_one']) : ?>
							<img src='<?php echo $args['image_one']['url']; ?>'>
						<?php else : ?>
							<div class="svg-wrapper">
								<img src='<?php echo $args['svg_one']['url']; ?>'>
							</div>
						<?php endif; ?>
						<?php echo $args['content_one']; ?>
						<?php if($args['button_one']) : ?>
							<a class='atc-button' href='<?php echo $args['button_one']['url']; ?>'><?php echo $args['button_one']['title']; ?></a>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if('two' == $args['col_num']) : ?>
				<div class="gc-wrap two">
					<div class="gc-content generic">
						<?php if($args['modify_one']) : ?>
							<img src='<?php echo $args['image_one']['url']; ?>'>
						<?php else: ?>
							<div class="svg-wrapper">
								<img class="svg" src='<?php echo $args['svg_one']['url']; ?>'>
							</div>
						<?php endif; ?>
						<?php echo $args['content_one']; ?>
						<?php if($args['button_one']) : ?>
							<a class='atc-button' href='<?php echo $args['button_one']['url']; ?>'><?php echo $args['button_one']['title']; ?></a>
						<?php endif; ?>
					</div>
				</div>
				<div class="gc-wrap two second">
					<div class="gc-content generic">
						<?php if($args['modify_two']) : ?>
							<img src='<?php echo $args['image_two']['url']; ?>'>
						<?php else: ?>
							<div class="svg-wrapper">
								<img class="svg" src='<?php echo $args['svg_two']['url']; ?>'>
							</div>
						<?php endif; ?>
						<?php echo $args['content_two']; ?>
						<?php if($args['button_two']) : ?>
							<a class='atc-button' href='<?php echo $args['button_two']['url']; ?>'><?php echo $args['button_two']['title']; ?></a>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if('three' == $args['col_num']) : ?>
				<div class="gc-wrap three">
					<div class="gc-content generic">
						<?php if($args['modify_one']) : ?>
							<img src='<?php echo $args['image_one']['url']; ?>'>
						<?php else: ?>
							<div class="svg-wrapper">
								<img class="svg" src='<?php echo $args['svg_one']['url']; ?>'>
							</div>
						<?php endif; ?>
						<?php echo $args['content_one']; ?>
						<?php if($args['button_one']) : ?>
							<a class='atc-button' href='<?php echo $args['button_one']['url']; ?>'><?php echo $args['button_one']['title']; ?></a>
						<?php endif; ?>
					</div>
				</div>
				<div class="gc-wrap three second">
					<div class="gc-content generic">
						<?php if($args['modify_two']) : ?>
							<img src='<?php echo $args['image_two']['url']; ?>'>
						<?php else: ?>
							<div class="svg-wrapper">
								<img class="svg" src='<?php echo $args['svg_two']['url']; ?>'>
							</div>
						<?php endif; ?>
						<?php echo $args['content_two']; ?>
						<?php if($args['button_two']) : ?>
							<a class='atc-button' href='<?php echo $args['button_two']['url']; ?>'><?php echo $args['button_two']['title']; ?></a>
						<?php endif; ?>
					</div>
				</div>
				<div class="gc-wrap three third">
					<div class="gc-content generic">
						<?php if($args['modify_three']) : ?>
							<img src='<?php echo $args['image_three']['url']; ?>'>
						<?php else: ?>
							<div class="svg-wrapper">
								<img class="svg" src='<?php echo $args['svg_three']['url']; ?>'>
							</div>
						<?php endif; ?>
						<?php echo $args['content_three']; ?>
						<?php if($args['button_three']) : ?>
							<a class='atc-button' href='<?php echo $args['button_three']['url']; ?>'><?php echo $args['button_three']['title']; ?></a>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>