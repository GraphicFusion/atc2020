<?php
	global $pilot;
	// add module layout to flexible content 
	$name = "generic_content";
	$module_layout = array (
		'key' => create_key('generic_content','block'),
		'name' => 'generic_content_block',
		'label' => 'Generic Content',
		'display' => 'block',
		'sub_fields' => array (
			array(
		        'key' => create_key($name,'num'),
				'label' => 'Number of Columns',
				'name' => $name . '_block_num',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'one' => 'One',
					'two' => 'Two',
					'three' => 'Three',
				),
				'default_value' => array(
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'return_format' => 'value',
				'ajax' => 0,
				'placeholder' => '',
			),			
            array(
                'key' => create_key($name,'modify_one'),
                'label' => 'First Column: Use Image or SVG',
                'name' => $name . '_block_modify_one',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'one',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 1,
				'ui' => 1,
				'ui_on_text' => 'Image',
				'ui_off_text' => 'SVG',                
            ),			
            array(
                'key' => create_key($name,'image_one'),
                'label' => 'First Column: Image',
                'name' => $name . '_block_image_one',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify_one'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'one',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name,'modify_one'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name,'modify_one'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => create_key($name,'svg_one'),
                'label' => 'First Column: SVG',
                'name' => $name .'_block_svg_one',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify_one'),
                            'operator' => '==',
                            'value' => '0',
                        ),
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'one',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name,'modify_one'),
                            'operator' => '==',
                            'value' => '0',
                        ),
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name,'modify_one'),
                            'operator' => '==',
                            'value' => '0',
                        ),
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
			array (
				'key' => create_key($name,'button_one'),
				'label' => 'First Column: Button',
				'name' => $name . '_block_button_one',
				'type' => 'link',
				'instructions' => '',
				'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'one',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
				'wrapper' => array(
					'width' => '33%',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
			),


			array (
		        'key' => create_key('generic_content','content_one'),
				'label' => 'First Column: Content',
				'name' => 'generic_content_block_content_one',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'one',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name . '_block_num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),

            array(
                'key' => create_key($name,'modify_two'),
                'label' => 'Second Column: Use Image or SVG',
                'name' => $name . '_block_modify_two',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name, 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 1,
				'ui' => 1,
				'ui_on_text' => 'Image',
				'ui_off_text' => 'SVG',                
            ),			
            array(
                'key' => create_key($name,'image_two'),
                'label' => 'Second Column: Image',
                'name' => $name . '_block_image_two',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify_two'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name,'modify_two'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => create_key($name,'svg_two'),
                'label' => 'Second Column: SVG',
                'name' => $name . '_block_svg_two',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify_two'),
                            'operator' => '==',
                            'value' => '0',
                        ),
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name,'modify_two'),
                            'operator' => '==',
                            'value' => '0',
                        ),
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
			array (
				'key' => create_key($name,'button_two'),
				'label' => 'Second Column: Button',
				'name' => $name . '_block_button_two',
				'type' => 'link',
				'instructions' => '',
				'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
				'wrapper' => array(
					'width' => '33%',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
			),


			array (
		        'key' => create_key('generic_content','content_two'),
				'label' => 'Second Column: Content',
				'name' => 'generic_content_block_content_two',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'two',
                        ),
                    ),
                    array(
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
            array(
                'key' => create_key($name,'modify_three'),
                'label' => 'Third Column: Use Image or SVG',
                'name' => $name . '_block_modify_three',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name, 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 1,
				'ui' => 1,
				'ui_on_text' => 'Image',
				'ui_off_text' => 'SVG',                
            ),			
            array(
                'key' => create_key($name,'image_three'),
                'label' => 'Third Column: Image',
                'name' => $name . '_block_image_three',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify_three'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => create_key($name,'svg_three'),
                'label' => 'Third Column: SVG',
                'name' => $name . '_block_svg_three',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify_three'),
                            'operator' => '==',
                            'value' => '0',
                        ),
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
			array (
				'key' => create_key($name,'button_three'),
				'label' => 'Third Column: Button',
				'name' => $name . '_block_button_three',
				'type' => 'link',
				'instructions' => '',
				'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
				'wrapper' => array(
					'width' => '33%',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
			),
			array (
		        'key' => create_key('generic_content','content_three'),
				'label' => 'Third Column: Content',
				'name' => 'generic_content_block_content_three',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name , 'num'),
                            'operator' => '==',
                            'value' => 'three',
                        ),
                    ),
                ),
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),

		),
		'min' => '',
		'max' => '',
	);
?>