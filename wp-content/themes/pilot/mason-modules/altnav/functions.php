<?php
	function build_altnav_layout(){
		$args = array(
			'main_links' => mason_get_sub_field('altnav_block_main_nav')
		);

		$name = "altnav";
		if(get_sub_field($name.'_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field($name.'_block_margin-top');
		}
		if(get_sub_field($name.'_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field($name.'_block_margin-bottom');
		}
		return $args;
	}
?>