<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
	$style = get_field('styling_options', 'option') ;

?>
<div class="altnav" id="altnav_id">
	<div class="container container-lg">
		<div class="row">
			<div class="col-xs-12">
				<div class="main-nav">
					<?php $active_link_subs = ""; if( count($args['main_links'])>0): ?>
						<ul class="primary-nav">
							<?php foreach($args['main_links'] as $main): 
								$main_href = $main['altnav_block_main_link']['url'];
								$active_class = "";
								$active_link = 0;
								if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
									$active_class = " active";
									$active_link = 1;
								}
								$is_parent_class ="";
							?>
								<?php 
									$subs = "";
									if( array_key_exists('altnav_block_sublinks',$main) && is_array($main['altnav_block_sublinks']) && count($main['altnav_block_sublinks'])>0): 
										$is_parent_class = "menu-item-has-children";
								?>
									<?php foreach($main['altnav_block_sublinks'] as $sub ) : 
										if( isset($sub['altnav_block_sub_link']['url'])){
											$sub_href = $sub['altnav_block_sub_link']['url'];
											if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
												$active_class = " active";
												$active_link = 1;
											}
											$subs .= '<li><a href="'.$sub_href.'">'.$sub['altnav_block_sub_link']['title'].'</a></li>';
										}
									endforeach; ?>
								<?php 
									if( $active_link && $subs){
										$active_link_subs = $subs;
									}
									endif; 
								?>
								<li class="<?php echo $active_class. " ". $is_parent_class." ".$style; ?>"><a href="<?php echo $main_href; ?>">
										<?php echo $main['altnav_block_main_link']['title']; ?>
									</a>
									<?php if($subs) : ?>
										<ul class="sub-nav">
											<li class="main-link">
												<a href="<?php echo $main_href; ?>">
													<?php echo $main['altnav_block_main_link']['title']; ?>
												</a>
											</li>
											<?php echo $subs; ?>
										</ul>
									<?php endif; ?>
								</li>
							<?php endforeach; ?>
								<li class="overflow-dots" style="display:none; margin-right:0; padding-left:0;">
									<img class="menu-overflow" src="<?php echo get_template_directory_uri(); ?>/mason-modules/altnav/images/Icon-Dots.svg">	
									<ul class="sub-nav">

									</ul>					
								</li>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>

	</div>
</div>
<style>
	.overflown{ display:none !important; }
</style>
<script>
	jQuery(document).ready(function($) {
		var $altnav = $('#altnav_id');
		if( $altnav.length > 0 ){
			var $container = $altnav.find( '.primary-nav' ),
				dotWidth = $altnav.find( '.overflow-dots' ).outerWidth(),
				totalLength = ( $container.width() - dotWidth),
				length = 0,
				greatestLength = 0,
				$items = $container.children('li').not('.overflow-dots'),
				overflow = [];

//console.log('totallLength', totalLength);
				$.each( $items, function(a,b){
					var $test = $( b ).find('a').wrap( "<span></span>" );
					var itemLength = $test.outerWidth();
					length += itemLength + 30;
					//console.log('test' +  $( b ).find('a').html(),length);

					if( length > (totalLength )){
						if( itemLength > greatestLength ){
							greatestLength = itemLength;
						}
						var $bClone = $(b).clone();
						$(b).addClass('overflown');
						overflow.push($bClone);
						$('.overflow-dots').show();

						//console.log('overflow dots show and remove b - 3:11', $(b));
//						console.log(' b ', $(b) );
					}
//					console.log('a:' + a, b);
				})
				$.each(overflow,function(k,v){
					$('.overflow-dots .sub-nav').append(v);
					$('.overflow-dots .sub-nav li').width(greatestLength + 100 );
				})
//			alert( $container.width() );
		}
		$('body').click(function(e){
			var target = $(e.target);
			//console.log('body click target:' , target);
			if( !target.hasClass('overflow-dots') ){
					$('.overflow-dots').removeClass('open-menu');
			//		console.log('body click remove open-menu');
			}
		})
		$('.overflow-dots').click(function(){
//			alert(1);
			if( $('.overflow-dots').hasClass('open-menu') ){
				//console.log('remove open-menu');
				$('.overflow-dots').removeClass('open-menu');
			}
			else{
				//console.log('add open-menu');
				$('.overflow-dots').addClass('open-menu');
			}
		});
		$('h1').on('click',function(){
				$.each( $('#altnav_id .primary-nav').children('li'), function(a,b){
					var $test = $( b ).find('a').wrap( "<span></span>" );
					console.log('test',$test.outerWidth());
				});
		})
	})
</script>