<?php

	global $pilot;
	// add module layout to flexible content 
	$name = "halfhalf";
	$style_prefix = get_field('styling_options', 'option');
	if('default' == $style_prefix || !$style_prefix){
		$choices = [
            'light' => 'Light',
            'dark' => 'Dark'			
		];
	}
	else{
		$choices = [
            'white' => 'White',
            'blue' => 'Blue',
            'orange' => 'Orange'			
		];
	}

	$module_layout = array (
		'key' => create_key($name, 'block'),
		'name' => 'halfhalf_block',
		'label' => 'Half and Half',
		'display' => 'block',
		'sub_fields' => array (
			array(
	            'key' => create_key('halfhalf','posititon'),
	            'label' => 'Image on Left or Right',
	            'name' => $name . '_block_left',
	            'type' => 'radio',
	            'instructions' => '',
	            'required' => 0,
	            'conditional_logic' => 0,
	            'wrapper' => array(
	                'width' => '50%',
	                'class' => '',
	                'id' => '',
	            ),
	            'choices' => array(
	                'left' => 'Left',
	                'right' => 'Right',
	            ),
	            'other_choice' => 0,
	            'save_other_choice' => 0,
	            'default_value' => '',
	            'layout' => 'horizontal',
	        ),
			array(
	            'key' => create_key('halfhalf','background'),
	            'label' => 'Background',
	            'name' => 'halfhalf_block_background',
	            'type' => 'radio',
	            'instructions' => '',
	            'required' => 0,
	            'conditional_logic' => 0,
	            'wrapper' => array(
	                'width' => '50%',
	                'class' => '',
	                'id' => '',
	            ),
	            'choices' => $choices,
	            'other_choice' => 0,
	            'save_other_choice' => 0,
	            'default_value' => '',
	            'layout' => 'horizontal',
	        ),
			array (
		        'key' => create_key($name,'content'),
				'label' => 'Content',
				'name' => $name . '_block_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),	        
			array (
				'key' => create_key($name,'button'),
				'label' => 'Button',
				'name' => $name . '_block_button',
				'type' => 'link',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '33%',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
			),
			array (
				'key' => create_key($name,'image'),
				'label' => 'Image',
				'name' => $name . '_block_image',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '33%',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
            array(
                'key' => create_key($name,'youtube'),
                'label' => 'Popup Link (YouTube)',
                'name' => $name . '_block_youtube',
                'type' => 'text',
                'instructions' => 'in the form: http://www.youtube.com/watch?v=cFdCzN7RYbw',
                'required' => 0,
                'conditional_logic' => array(
                ),
                'wrapper' => array(
                    'width' => '33%',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
		),
		'min' => '',
		'max' => '',
	);
?>