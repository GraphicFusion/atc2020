<?php
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
	$block_id = $args['acf_incr'];
	$btn_class = '';
	if( 'dark' == $args['background'] ){
		$btn_class ='dark-bg';
	}
?>
	<div class='slides-section'>
		<div class="feature-wrapper ">
			<div class='half-half slick-slider image-<?php echo $args['position']; ?>' id="slick-slider-<?php echo $block_id; ?>">
				<img class='slides-block-image' src='<?php echo $args['image']['url']; ?>'>
	  		</div>
	  		<div class="caption-wrapper image-<?php echo $args['position']; ?> <?php echo $args['background']; ?>">
				<div class="caption" >
					<div class="caption-inner-wrapper" >
						<?php echo $args['content']; ?>
						<?php if(isset($args['button']) && is_array($args['button']) && array_key_exists('url',$args['button'])) : ?>
							<a class="atc-button <?php echo $btn_class; ?>" href='<?php echo $args['button']['url']; ?>'><?php echo $args['button']['title']; ?></a>
						<?php endif; ?>
						<?php if(isset($args['youtube']) && $args['youtube']) : ?>
							<a class="video-btn " href="<?php echo $args['youtube']; ?>">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                    <path d="M20,0C9,0,0,9,0,20s9,20,20,20c11,0,20-9,20-20S31,0,20,0z M15.4,30V10l13.3,10L15.4,30z" />
                                </svg>
                            </a>							
						<?php endif; ?>
					</div>
				</div>
		  	</div><!--/caption-wrpper-->
		  	
		</div><!--/feature-wrapper-->
	</div><!--/slides-section-->
	<script>
		$(document).ready(function(){
			var $width = 959;
			var adaptiveHeight = false;
			if($(window).width() < $width ){
				adaptiveHeight = true;

			} 
//		  var $appendArrows = $('#nav-wrapper-mobile-<?php echo $block_id; ?>');
				  var $appendArrows = $('#nav-wrapper-<?php echo $block_id; ?>');
				if($(window).width() > $width ){
				}
	  $('.half-half#slick-slider-<?php echo $block_id; ?>').on('init', function(event, slick, currentSlide, nextSlide){
				if($(window).width() > $width ){
					var height = $('#halfhalf_block_<?php echo $block_id; ?>').find('.caption-inner-wrapper').outerHeight();
					var addPaddingTop = $('#halfhalf_block_<?php echo $block_id; ?>').find('.caption').css('padding-top');			//
					height = height + parseInt(addPaddingTop);
					if(height < 640 ){
						height = 640;
					}
					$('.half-half#slick-slider-<?php echo $block_id; ?>').closest('.feature-wrapper').height(height);
					$('.half-half#slick-slider-<?php echo $block_id; ?>').find('.slick-list').height(height);
					$('.half-half#slick-slider-<?php echo $block_id; ?>').find('img').height(height);
					$('.half-half#slick-slider-<?php echo $block_id; ?>').height(height);
					$('.half-half#slick-slider-<?php echo $block_id; ?>').find('.caption').height(height);
				}
				else{
					$.each( $('.half-half#slick-slider-<?php echo $block_id; ?> img'), function(k,v){
						var width = $(v).width(),
						height = width * 2/3;
						$(v).height(height);
					})
				}
	  })
	  $('.half-half#slick-slider-<?php echo $block_id; ?>').slick({
		slidesToShow:1,
		dots:false,
		arrows:true,
		appendArrows:$appendArrows,
		adaptiveHeight: true,
	  });


});
	</script>
