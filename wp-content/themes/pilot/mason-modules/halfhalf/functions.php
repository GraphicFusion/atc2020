<?php
	function build_halfhalf_layout(){
		$args = [];
		$args['position'] = mason_get_sub_field('halfhalf_block_left');
		$args['button'] = mason_get_sub_field('halfhalf_block_button');
		$args['background'] = mason_get_sub_field('halfhalf_block_background');
		$args['content'] = mason_get_sub_field('halfhalf_block_content');
		$args['image'] = mason_get_sub_field('halfhalf_block_image');
		$args['youtube'] = mason_get_sub_field('halfhalf_block_youtube');

		$name = "halfhalf";
		if(get_sub_field($name.'_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field($name.'_block_margin-top');
		}
		if(get_sub_field($name.'_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field($name.'_block_margin-bottom');
		}
		return $args;
	}
function wp_atc_enqueue_scripts() {
	wp_enqueue_script( 'slide_script', get_template_directory_uri() . "/mason-modules/halfhalf/script.js", array('jquery'), null, true );
	wp_enqueue_script( 'popup_script', get_template_directory_uri() . "/mason-modules/halfhalf/html5lightbox.js" , array('jquery'), null, true);
}
add_action( 'wp_enqueue_scripts', 'wp_atc_enqueue_scripts' );

?>