<!DOCTYPE html>
	<?php 
		global $pilot;
		get_template_part( 'views/head' ); 
	?>
	<body <?php body_class( get_field('styling_options', 'option') . '-style'); ?> >
		<?php echo $snippets = get_field('snippet_after_body', 'option'); ?>
		<div class="site drop-shadow" onclick="void(0);">
			<?php get_template_part( 'views/header' ); ?>
			<div class="site-content">
				<main class="site-main">