<?php
/**
 * Enqueue scripts and styles.
 */
function pilot_scripts() {
	$style_prefix = get_field('styling_options', 'option');
	if('default' == $style_prefix || !$style_prefix){
		$style_prefix = "";
	}
	else{
		$style_prefix = $style_prefix."-";
	}
    wp_enqueue_style( 'pilot-style', get_template_directory_uri().'/dest/css/'.$style_prefix.'main.min.css');

	wp_deregister_script('jquery');
	wp_register_script('jquery', ('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'), false, '2.1.4', true);
	wp_enqueue_script( 'pilot-libraries', get_template_directory_uri() . '/dest/js/lib.min.js', array('jquery'), '20120206', true );
    wp_enqueue_script( 'pilot-scripts', get_template_directory_uri() . '/dest/js/app.min.js', array('jquery'), '20120206', true );
	wp_enqueue_style( 'slick_style',"https://kenwheeler.github.io/slick/slick/slick-theme.css");
	wp_enqueue_style( 'slick_theme_style',"https://kenwheeler.github.io/slick/slick/slick.css");
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'pilot_scripts' );

function load_pilot_admin_style() {
//        wp_register_style( 'pilot_admin_css', get_template_directory_uri() . '/includes/modules/admin-modules.css', false, '1.0.0' );
  //      wp_enqueue_style( 'pilot_admin_css' );
	//	wp_enqueue_script( 'pilot_admin_js',  get_template_directory_uri() . '/includes/modules/admin-modules.js' , array('jquery'));
}
add_action( 'admin_enqueue_scripts', 'load_pilot_admin_style' );