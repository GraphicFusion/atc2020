jQuery(document).ready(function($) {
	function buildBlogSummary($post){
		var output =	'<div class="col-lg-12"><div class="img-block-wrap"><img class="blog-image" src="' + $post.url + '"></div><!--img-block-wrap--><div class="title-wrap"><div class="caption">' + $post.pretty_date + '</div><h3 class="blog-title">' + $post.post_title + '</h3><a class="atc-button" href="' + $post.permalink + '">Read</a></div><!--/title-wrap--></div><div class="col-lg-12 hr"><hr></div>';
		return output; 
	}
	function loadMore( $id ){
		$.ajax({
			url: ajax_pagination.ajaxurl,
			type: 'post',
			async:    true,
			cache:    false,
			dataType: 'json',			
			data: {
				action: 'ajax_pagination',
				params: {
					'paged': ajaxPage
				}
			},
			success: function( result ) {
				console.log( result);
				if(result.posts.length > 0){
					$.each(result.posts, function(k,post){
						console.log('POST',post);
						var div = buildBlogSummary(post);
						$('#' + $id).find('.blog-container').append(div);
						console.log('div' + $id,div);
					})
				}
				else{
					$('#' + $id).find('#load_more').html('All Posts Are Loaded').addClass('inactive');
				}				
				window.ajaxPage++;
			}
		})		
	}
	if('undefined' != typeof window.blogModules){
		$.each(window.blogModules, function(a,b){
			console.log('blogm:' + a,b);
			loadMore(b);
		})
		$('#load_more').click(function(e){
			e.preventDefault();
			var id = $(e.target).data('id');
			loadMore(id);
		})
	}
})