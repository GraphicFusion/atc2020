<?php get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="entry-content block-generic_content module    ">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="gc-wrap one">
									<div class="gc-content">
										<header class="entry-header">
											<div class="leader-wrap">
												<span>News</span><div class="date"><?php echo date( 'F j, Y', strtotime( $post->post_date ) ); ?></div>
											</div>
											<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
										</header>
										<?php echo get_the_post_thumbnail( $post, 'large'); ?>
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- .entry-content -->
			</article>
		<?php endwhile; ?>

<?php get_footer(); ?>