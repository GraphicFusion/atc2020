
# Pilot

Hi. I'm a starter theme called `Pilot`. I'm a theme meant for hacking so don't use me as a Parent Theme. Instead try turning me into the next, most awesome, WordPress theme out there. That's what I'm here for.

A small, curated library of variables and mixins provide a starting point for effective, waste-free development.

Pilot uses a modular system with drag-and-drop component folders located in includes/modules. These use Advanced Custom Fields for their data and some may require the ACF Pro to be used. SCSS and JS files for each module are configured via the parent folder and concatenated and minified using Grunt.

## Features

* [Grunt](http://gruntjs.com/) build script that compiles Sass, checks for JavaScript errors and concatenates and minifies files.
* [Bower](http://bower.io/) for front-end package management.
* [Susy](http://susy.oddbird.net/) for an optional grid.
* [Slick](https://kenwheeler.github.io/slick/) The greatest slider ever
* [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/) Versatile and effective responsive popup tool
* A just right amount of lean, well-commented, modern, HTML5 templates.
* A helpful 404 template.
* A sample custom header implementation in inc/custom-header.php that can be activated by uncommenting one line in functions.php and adding the code snippet found in the comments of inc/custom-header.php to your header.php template.
* Custom template tags in inc/template-tags.php that keep your templates clean and neat and prevent code duplication.
* Some small tweaks in inc/extras.php that can improve your theming experience.


## Getting Started

### Install Grunt and Bower

Building the theme requires [node.js](http://nodejs.org/download/). We recommend you update to the latest version of npm: `npm install -g npm@latest`.

From the command line:

1. Install [Grunt](http://gruntjs.com/) and [Bower](http://bower.io/) globally with `npm install -g grunt bower`
2. Navigate to the theme directory, then run `npm install`
3. Run `bower install`
4. To use Browsersync, run `npm install -g browser-sync`, and in Gruntfile.js update the Browsersync task 'proxy' field with whatever url your local server or VM is using
5. Run `grunt` or `grunt watch` and happy developing!
